import numpy as np
from keras.engine import Model
from keras.utils import multi_gpu_model


class ModelMGPU(Model):
    def __init__(self, model, gpus):
        pmodel = multi_gpu_model(model, gpus)
        self.__dict__.update(pmodel.__dict__)
        self._smodel = model

    def __getattribute__(self, attrname):
        '''Override load and save methods to be used from the serial-model. The
        serial-model holds references to the weights in the multi-gpu model.
        '''
        # return Model.__getattribute__(self, attrname)
        if 'load' in attrname or 'save' in attrname:
            return getattr(self._smodel, attrname)

        return super(ModelMGPU, self).__getattribute__(attrname)