import os
import numpy as np
import nibabel as nib
from utils import crop_img
from pathlib import Path



if __name__ == '__main__':
    dir_list = next(os.walk('./data/'))[1]
    # HGG_dir_list = next(os.walk('./HGG/'))[1]
    # LGG_dir_list = next(os.walk('./LGG/'))[1]


    slice = [[300,0],[300,0],[300,0]]
    for foldername in dir_list:
        folder = os.listdir ('./data/' + foldername)
        for file in folder:
            if ('t1'  in file) or ('t1ce'  in file) or ('t2'  in file) or ('flair'  in file):
                file_path = './data/' + foldername + '/'+ file
                img = nib.load (file_path)
                croped_slice = crop_img (img)
                for j in range (3):
                    slice [j][0] = np.minimum ( slice [j][0]  , croped_slice[j].start)
                    slice [j][1] = np.maximum ( slice [j][1] , croped_slice[j].stop )

    # for foldername in LGG_dir_list:
    #     folder = os.listdir ('./LGG/' + foldername)
    #     for file in folder:
    #         if ('t1'  in file) or ('t1ce'  in file) or ('t2'  in file) or ('flair'  in file):
    #             file_path = './LGG/' + foldername + '/'+ file
    #             croped_slice = crop_img (file_path)
    #             for j in range (3):
    #                 a= croped_slice[j].start
    #                 b = slice [2][0]
    #                 slice [j][0] = np.minimum ( slice [j][0]  , croped_slice[j].start)
    #                 slice [j][1] = np.maximum ( slice [j][1] , croped_slice[j].stop )                
            
           
        
    print (slice)
