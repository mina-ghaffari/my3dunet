import os
import numpy as np
import nibabel as nib
from nipype.interfaces.ants import N4BiasFieldCorrection
from nilearn.image.image import check_niimg
from nilearn.image.image import _crop_img_to as crop_img_to




def bias_correct (in_folder, out_folder):
    img1 = in_folder  + '/' + in_folder.split('/')[2] + '_flair.nii.gz'
    img2 = in_folder  + '/' + in_folder.split('/')[2]  + '_t1.nii.gz'
    img3 = in_folder  + '/' + in_folder.split('/')[2] + '_t1ce.nii.gz'
    img4 = in_folder  + '/' + in_folder.split('/')[2] + '_t2.nii.gz'


    img1_out = out_folder   + '/' + out_folder.split('/')[3] + '_flair.nii.gz'
    img2_out = out_folder   + '/' + out_folder.split('/')[3] + '_t1.nii.gz'
    img3_out = out_folder   + '/' + out_folder.split('/')[3] + '_t1ce.nii.gz'
    img4_out = out_folder   + '/' + out_folder.split('/')[3] + '_t2.nii.gz'


    correct = N4BiasFieldCorrection()
    correct.inputs.input_image = img1
    correct.inputs.output_image = img1_out
    correct.run()

    correct.inputs.input_image = img2
    correct.inputs.output_image = img2_out
    correct.run()

    correct.inputs.input_image = img3
    correct.inputs.output_image = img3_out
    correct.run()

    correct.inputs.input_image = img4
    correct.inputs.output_image = img4_out
    correct.run()



if __name__ == '__main__':
    HGG_dir_list = next(os.walk('./HGG/'))[1]
    LGG_dir_list = next(os.walk('./LGG/'))[1]
    i = 0
    for folder in HGG_dir_list:
        HGG_dir_in  = './HGG/' + folder
        HGG_dir_out = './Preprocessed/HGG/' + folder
        print ('bias_correcting %d of  %d HGG_volumes' %(i, len(HGG_dir_list)) )

        if not os.path.exists (HGG_dir_out):
            os.mkdir (HGG_dir_out)
        bias_correct (HGG_dir_in, HGG_dir_out)
        i +=1


    j = 0
    for folder in LGG_dir_list:
        LGG_dir_in = './LGG/' + folder
        LGG_dir_out = './Preprocessed/LGG/' + folder
        print ('bias_correcting %d of  %d LGG_volumes' %(j, len(LGG_dir_list)) )
        if not os.path.exists (LGG_dir_out):
            os.mkdir (LGG_dir_out)
        bias_correct (HGG_dir_in, LGG_dir_out)  
        j +=1  

