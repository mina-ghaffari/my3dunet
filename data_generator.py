# https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly.html

import numpy as np
import keras
import nibabel as nib
from utils import crop_img
from test_train_partition import Partition_data
from keras.utils import np_utils


class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, batch_size=2, dim=(240,240,155), n_channels=4,
                 n_classes=3, shuffle=True):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, Y = self.__data_generation(list_IDs_temp)

        return X, Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.empty((self.batch_size, self.n_channels, *self.dim))
        Y = np.empty((self.batch_size, self.n_classes,  *self.dim))

       

        # Generate data
        # Decode and load the data
        for i, ID in enumerate(list_IDs_temp):

            img1 = './data/' + ID + '/'+ ID + '_flair.nii.gz'
            img2 = './data/' + ID + '/'+ ID + '_t1.nii.gz'
            img3 = './data/' + ID + '/'+ ID + '_t1ce.nii.gz'
            img4 = './data/' + ID + '/'+ ID + '_t2.nii.gz'
            img5 = './data/' + ID + '/'+ ID + '_seg.nii.gz'

            newimage = nib.concat_images([img1, img2, img3, img4, img5])
            cropped = crop_img(newimage)         
            img_array = np.array(cropped.dataobj)
            z = np.rollaxis(img_array, 3, 0)

            padded_image = np.zeros((5,160,192,160))
            padded_image[:z.shape[0],:z.shape[1],:z.shape[2],:z.shape[3]] = z

            a,b,c,d,seg_mask = np.split(padded_image, 5, axis=0)

            images = np.concatenate([a,b,c,d], axis=0)
            
            seg_mask_1 = np.zeros((1,160,192,160))
            seg_mask_1[seg_mask.astype(int) == 1] = 1
            seg_mask_2 = np.zeros((1,160,192,160))
            seg_mask_2[seg_mask.astype(int) == 2] = 1
            seg_mask_3 = np.zeros((1,160,192,160))
            seg_mask_3[seg_mask.astype(int) == 4] = 1            
            seg_mask_3ch = np.concatenate([seg_mask_1,seg_mask_2,seg_mask_3], axis=0).astype(int)
            
            # 1) the "enhancing tumor" (ET), 2) the "tumor core" (TC), and 3) the "whole tumor" (WT) 
            # The ET is described by areas that show hyper-intensity in T1Gd when compared to T1, but also when compared to “healthy” white matter in T1Gd. The TC describes the bulk of the tumor, which is what is typically resected. The TC entails the ET, as well as the necrotic (fluid-filled) and the non-enhancing (solid) parts of the tumor. The appearance of the necrotic (NCR) and the non-enhancing (NET) tumor core is typically hypo-intense in T1-Gd when compared to T1. The WT describes the complete extent of the disease, as it entails the TC and the peritumoral edema (ED), which is typically depicted by hyper-intense signal in FLAIR.
            # The labels in the provided data are: 
            # 1 for NCR & NET (necrotic (NCR) and the non-enhancing (NET) tumor core) = TC ("tumor core")
            # 2 for ED ("peritumoral edema")
            # 4 for ET ("enhancing tumor")
            # 0 for everything else

            X[i,] = images
            Y[i,] = seg_mask_3ch
        return X, Y







if __name__ == '__main__':
    params = {'dim': (160,192,160),
          'batch_size': 2,
          'n_classes': 3,
          'n_channels': 4,
          'shuffle': True}
    partition = Partition_data ()

    training_generator = DataGenerator(partition['train'], **params)
    validation_generator = DataGenerator(partition['test'], **params)