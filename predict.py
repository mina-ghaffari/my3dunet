import os
import numpy as np
from keras import backend as K
from keras.engine import Model
from keras.optimizers import Adam
from keras.models import Sequential
from keras.callbacks import EarlyStopping, ModelCheckpoint


from model import isensee2017_model
from metrics import dice_coefficient, dice_coefficient_loss, weighted_dice_coefficient, weighted_dice_coefficient_loss, label_wise_dice_coefficient
from data_generator import DataGenerator
from test_train_partition import Partition_data

import nibabel as nib
from utils import crop_img

K.set_image_dim_ordering('tf')
K.set_image_data_format('channels_first')




### model instantiation
model = isensee2017_model(input_shape=(4, 160, 192, 160), n_base_filters=8, depth=5, dropout_rate=0.3,
                      n_segmentation_levels=3, n_labels=3, optimizer=Adam, initial_learning_rate=5e-4,
                      loss_function=weighted_dice_coefficient_loss, activation_name="sigmoid")


# Validation Data and Parameters setting
params = {'dim': (160,192,160),
          'batch_size': 1,
          'n_classes': 3,
          'n_channels': 4,
          'shuffle': True}

partition = Partition_data ()          


# Generators
validation_generator = DataGenerator(partition['holdout'], **params)
ID = validation_generator.list_IDs [0]
img1 = './data/' + ID + '/'+ ID + '_flair.nii.gz'
img2 = './data/' + ID + '/'+ ID + '_t1.nii.gz'
img3 = './data/' + ID + '/'+ ID + '_t1ce.nii.gz'
img4 = './data/' + ID + '/'+ ID + '_t2.nii.gz'
img5 = './data/' + ID + '/'+ ID + '_seg.nii.gz'

vol = nib.load (img1)
affine = vol.affine


newimage = nib.concat_images([img1, img2, img3, img4, img5])
cropped, slices  = crop_img(newimage)         
img_array = np.array(cropped.dataobj)
z = np.rollaxis(img_array, 3, 0)



padded_image = np.zeros((5,160,192,160))
padded_image[:z.shape[0],:z.shape[1],:z.shape[2],:z.shape[3]] = z

a,b,c,d,seg_mask = np.split(padded_image, 5, axis=0)
images = np.concatenate([a,b,c,d], axis=0)

X = np.empty((1,4, 160,192,160))
X[0,] = images

model.load_weights ('weights_50.h5')
Y_pred = model.predict(X , verbose=1)

Y = np.zeros (Y_pred.shape , dtype=np.uint8)
Y [Y_pred > 0.5] = 1



seg_mask = np.zeros((160,192,160), dtype=np.uint8)
seg_mask = np.maximum ( Y [0,0,:,:,:] , 2 * Y[0,1,:,:,:] )
seg_mask = np.maximum ( seg_mask , 4*Y[0,2,:,:,:] )


label = np.zeros ((240, 240, 155))
label [slices[0].start:slices[0].start+seg_mask.shape[0], slices[1].start:slices[1].start+seg_mask.shape[1] , slices[2].start:slices[2].start+ seg_mask.shape[2]] = seg_mask


mask = nib.nifti1.Nifti1Image(label, affine) 
pred = './data/' + ID + '/'+ ID + '_pred_50epoch.nii.gz'
nib.save (mask, pred)




model.load_weights ('weights_4.h5')
Y_pred = model.predict(X , verbose=1)

Y = np.zeros (Y_pred.shape , dtype=np.uint8)
Y [Y_pred > 0.5] = 1


seg_mask = np.zeros((160,192,160), dtype=np.uint8)
seg_mask = np.maximum ( Y [0,0,:,:,:] , 2 * Y[0,1,:,:,:] )
seg_mask = np.maximum ( seg_mask , 4*Y[0,2,:,:,:] )

mask = nib.nifti1.Nifti1Image(seg_mask, affine) 
pred = './data/' + ID + '/'+ ID + '_pred_4epochs.nii.gz'
nib.save (mask, pred)





print (seg_mask.shape)

