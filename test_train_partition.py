import os
import numpy as np

def Partition_data (holdout_percentage =0.15, train_percentage=0.7):
    # HGG_dir_list = next(os.walk('./HGG/'))[1]
    # # print(len(HGG_dir_list))
    # LGG_dir_list = next(os.walk('./LGG/'))[1]
    # # print(len(LGG_dir_list))


    # completelist = HGG_dir_list + LGG_dir_list
    completelist = next(os.walk('./data/'))[1]
    # print(completelist[0:4])
    np.random.shuffle(completelist) # shuffles in place
    # print(completelist[0:4])

    partition={}

    partition['holdout']=completelist[0:int(len(completelist)*holdout_percentage)]
    trainlist=completelist[int(len(completelist)*holdout_percentage):len(completelist)]

    partition['train']=trainlist[0:int(len(trainlist)*train_percentage)]
    partition['test']=trainlist[int(len(trainlist)*train_percentage):len(trainlist)]


    # labels={}
    # # HGG=0
    # # LGG=1
    # for directory in HGG_dir_list:
    #     labels[directory]=0
    # for directory in LGG_dir_list:
    #     labels[directory]=1
        
    # print(len(partition['holdout']))
    # print(len(partition['train']))
    # print(len(partition['test']))

    return partition

if __name__ == "__main__":
    partition = Partition_data ()

    print(len(partition['holdout']))
    print(len(partition['train']))
    print(len(partition['test']))

