import os
import numpy as np
from keras import backend as K
from keras.engine import Model
from keras.optimizers import Adam
from keras.models import Sequential
from keras.callbacks import EarlyStopping, ModelCheckpoint

K.set_image_dim_ordering('tf')
K.set_image_data_format('channels_first')



from model import isensee2017_model
from metrics import dice_coefficient, dice_coefficient_loss, weighted_dice_coefficient, weighted_dice_coefficient_loss, label_wise_dice_coefficient
from data_generator import DataGenerator
from test_train_partition import Partition_data




### model instantiation
model = isensee2017_model(input_shape=(4, 160, 192, 160), n_base_filters=8, depth=5, dropout_rate=0.3,
                      n_segmentation_levels=3, n_labels=3, optimizer=Adam, initial_learning_rate=5e-4,
                      loss_function=weighted_dice_coefficient_loss, activation_name="sigmoid")
model.summary()







# Trainig  Data and Parameters setting
params = {'dim': (160,192,160),
          'batch_size': 1,
          'n_classes': 3,
          'n_channels': 4,
          'shuffle': True}

partition = Partition_data ()          


# Generators
training_generator   = DataGenerator(partition['train'], **params)
validation_generator   = DataGenerator(partition['test'], **params)

# # Design model
# model = Sequential()
# [...] # Architecture
# model.compile()

# Train model on dataset
# model.fit_generator(generator=training_generator,
#                     validation_data=validation_generator,
#                     use_multiprocessing=True,
#                     workers=4)

cb_1=EarlyStopping(monitor='val_loss', min_delta=0, patience=2, verbose=1, mode='auto')

# cb_2=keras.callbacks.ModelCheckpoint(filepath=weights.{epoch:02d}-{val_loss:.2f}.hdf5, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
cb_2=ModelCheckpoint(filepath='weights.h5', monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
                                              
results = model.fit_generator(generator=training_generator,
                    validation_data=validation_generator,
                   epochs=200, 
                   nb_worker=4,
                   callbacks=[cb_1,cb_2])
                #    callbacks=[cb_2])
print("Test-Accuracy:", np.mean(results.history["val_acc"]))








# model.fit_generator(
#         generator=training_generator,
#         samples_per_epoch=2,
#         steps_per_epoch=2000,
#         epochs=50,
#         validation_data=validation_generator,
#         nb_worker=1,
#         validation_steps=800)

